from enum import Enum


class Repository(object):
    def __init__(self, repository_id, repository_type, name, owner, description, stars, language, updated):
        """
        :param repository_id: identifier of remote github repository.
        :type repository_id: int

        :param repository_type: type of repository.
        :type repository_type: application.repositories.domain.Repositories

        :param owner: owner of the repository.
        :type owner: str

        :param name: name of the repository. ex: owner/`repository`.
        :type name: str

        :param description: short specification information about repository.
        :type description: str

        :param stars: number of stars for repository.
        :type stars: int

        :param language: repository language code. ex `Python` or `Java`
        :param language: str

        :param updated: date of last update repository.
        :param updated: datetime
        """
        self.repository_id = repository_id
        self.repository_type = repository_type
        self.name = name
        self.owner = owner
        self.description = description
        self.stars = stars
        self.language = language
        self.updated = updated

    def json(self):
        return dict(
            name=self.name,
            repository_type=Repositories(self.repository_type).value,
            description=self.description,
            stars=self.stars,
            language=self.language,
            updated=self.updated
        )


class Repositories(Enum):
    GITHUB = 13
    GITLAB = 14
    BITBUCKET = 17
