from core.helpers import Pagination, OrderSortGroup
from core.tools import Handler
from application import REPOSITORIES_MODEL, REPOSITORIES_CLIENT


class OnGetRecent(Handler):
    @staticmethod
    def decode(params):
        return Pagination.validate(params), \
               OrderSortGroup.validate(params, fields=['stars'])

    @staticmethod
    def handle(params):
        pagination, (order, sort, group) = params
        return REPOSITORIES_MODEL.get_recent(pagination=pagination)

    @staticmethod
    def encode(result):
        pagination, recents = result
        return {
            'meta': pagination.json(),
            'recents': [r.json() for r in recents]
        }


class OnGetSearch(Handler):
    @staticmethod
    def decode(params):
        return params.get("keyword"), Pagination.validate(params), OrderSortGroup.validate(params)

    @staticmethod
    def handle(params):
        keyword, pagination, (order, group, sort) = params
        return pagination, REPOSITORIES_MODEL.search(keyword=keyword, skip=pagination.skip, limit=pagination.limit)

    @staticmethod
    def encode(result):
        pagination, repositories = result
        return {
            'meta': pagination.json(),
            'items': [r.json() for r in repositories]
        }


class OnGetSearchRemote(Handler):
    @staticmethod
    def decode(params):
        return params.get("query", None), params.get("language", None), params.get("stars", None), \
               Pagination.validate(params)

    @staticmethod
    def handle(params):
        query, language, stars, paginate = params
        repos = REPOSITORIES_CLIENT.search_repositories(query, language=language, stars=stars, pagination=paginate)
        REPOSITORIES_MODEL.upsert(repos)
        return paginate, repos

    @staticmethod
    def encode(result):
        pagination, repositories = result
        return {
            'meta': pagination.json(),
            'items': [r.json() for r in repositories]
        }
