import logging

from twisted.web.resource import Resource
from twisted.web.server import Site
from twisted.internet import reactor
from twisted.python import log


from application.repositories.routes import rules


def application(environ, start_response):
    start_response('200 OK', [('Content-Type', 'text/html')])
    return ["<h1 style='color:blue'>Hello There!</h1>"]

class Repositories(Resource):
    isLeaf = True

    def render_GET(self, request):
        route = request.path.decode('utf8')
        log.msg("Don't mind", logLevel=logging.DEBUG)
        if route in rules:
            body = rules[route].handler(request.args)()
            request.setResponseCode(200)
            request.setHeader("Content-Type", "application/json; charset=utf-8")
            return body.encode('utf-8')


reactor.listenTCP(8080, Site(Repositories()))
reactor.run()

