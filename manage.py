import argparse

from application.repositories.routes import CommandLineRepositoriesRules
from settings import CONFIG
from core.exceptions import ValidationError
from framework.flask import app

__version__ = '1.0.1.4'

PARSER = argparse.ArgumentParser()
PARSER.add_argument(
    '-v', '--version', dest='show_version', action='store_true',
    help='Show application version',
)
PARSER.add_argument(
    '-d', '--dependencies', dest='install_dependencies', action='store_true',
    help='Install virtualenv and python dependencies',
)
PARSER.add_argument(
    '-o', '--host', dest='host', action='store', default=CONFIG.HOST,
    help='The host to start the application',
)
PARSER.add_argument(
    '-p', '--port', dest='port', action='store', default=CONFIG.PORT,
    help='The port to start the application',
)
PARSER.add_argument(
    '-s', '--search', dest='search', action='store_true',
    help='Infinity repositories search',
)
PARSER.add_argument(
    '-sl', '--search-local', dest='search_local', action='store_true',
    help='Local repositories search',
)
PARSER.add_argument(
    '-sr', '--search-remote', dest='search_remote', action='store_true',
    help='Remote repositories search',
)
PARSER.add_argument(
    '-q', '--keyword', '--query', dest='query', action='store', default=None,
    help='Remote repositories search',
)
PARSER.add_argument(
    '-t', '--type', dest='type', action='store', default=None,
    help='Type of repository',
)
PARSER.add_argument(
    '-l', '--limit', dest='limit', action='store', type=int, default=None,
    help='Search result limit',
)
PARSER.add_argument(
    '-pp', '--per-page', dest='per_page', action='store', type=int, default=100,
    help='Number of items on response page',
)
PARSER.add_argument(
    '-lng', '--language', dest='language', action='store', default="Python",
    help='Language of searches repositories',
)
PARSER.add_argument(
    '--stars', dest='stars', action='store', default=None,
    help='Count of stars repositories',
)
PARSER.add_argument(
    '--run', dest='run', action='store_true',
    help='Run application',
)
ARGS = PARSER.parse_args()


def __main__():
    for argument, value in vars(ARGS).items():
        if value and argument in CommandLineRepositoriesRules:
            try:
                result = CommandLineRepositoriesRules.get(argument)(vars(ARGS))
            except Exception as exception:
                print(exception)
    if ARGS.run:
        app.run(host=CONFIG.HOST, port=CONFIG.PORT, debug=CONFIG.DEBUG)


__main__()
