from voluptuous import Required, Schema, REMOVE_EXTRA, Any, Coerce, Invalid, MultipleInvalid, Match

from core.exceptions import ValidationError


class OrderSortGroupValidator(object):
    schema = Schema({
        Required('order', default="ASC"): Any("ASC", "DESC"),
        Required('group', default=None): Any(Match(r'[a-z,]*'), None),
        Required('sort', default=''): Match(r'[a-z,]*'),
    }, extra=REMOVE_EXTRA)

    @classmethod
    def validate(cls, params, fields=[]):
        try:
            params = cls.schema(params)
            params['sort'] = [field for field in params.get('sort', '').split(',') if field in fields] or None
            return params
        except Invalid as exception:
            raise ValidationError(parameter=exception.path, error=exception.error_message)


class PaginationValidator(object):
    schema = Schema({
        Required('count', default=0): Coerce(int),
        Required('skip', default=0): Coerce(int),
        Required('page', default=1): Coerce(int),
        Required('limit', default=100): Any(Coerce(int), None),
        Required('total', default=0): Coerce(int),
        Required('per_page', default=100): Any(Coerce(int), None),
    }, extra=REMOVE_EXTRA, )

    @classmethod
    def validate(cls, params):
        try:
            return cls.schema(params)
        except Invalid as exception:
            raise ValidationError(parameter=exception.path, error=exception.error_message)
