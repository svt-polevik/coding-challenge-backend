from application.repositories.client import RepositoriesClient
from application.repositories.dao import RepositoriesDAO
from application.repositories.model import RepositoriesModel
from application.repositories.clients.github.client import GitHubClient
from application.repositories.clients.gitlab.client import GitLabClient
from settings import CONFIG


_GITHUB_CLIENT = GitHubClient(CONFIG.GITHUB_ACCESS_TOKEN)
_REPOSITORIES_DAO = RepositoriesDAO()
REPOSITORIES_CLIENT = RepositoriesClient(clients=[_GITHUB_CLIENT])
REPOSITORIES_MODEL = RepositoriesModel(_REPOSITORIES_DAO)
