import os


class EnvironmentConfiguration:
    APP_NAME = "smashdocs"
    DEBUG = False
    DIR_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    DIR_MAIN = '/src'
    DIR_TEMP = 'temp'
    HOST = '127.0.0.1'
    PORT = 8080
    MONGO_HOST = '127.0.0.1'
    MONGO_PORT = 27017
    MONGO_DATABASE_NAME = 'repositories'
    GITHUB_DEFAULT_SEARCH_QUERY = 'language:python stars:>500'
    GITHUB_ACCESS_TOKEN = ''
