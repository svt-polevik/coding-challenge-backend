import unittest
from flexmock import flexmock

from application.repositories.model import RepositoriesModel
from application.repositories.tests.fixtures import RepositoryFactory


class RepositoriesModelTestCase(unittest.TestCase):
    def setUp(self):
        self.dao = flexmock()
        self.model = RepositoriesModel(self.dao)

    def test_ensure_that_get_recent_return_values(self):
        repositories = RepositoryFactory.build_batch(10)
        self.dao.should_receive("get_recent").and_return(len(repositories), repositories)
        pagination, repos = self.model.get_recent()
        assert repositories == repos
