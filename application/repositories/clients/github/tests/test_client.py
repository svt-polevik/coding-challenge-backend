import json
import unittest
import httpretty
import os

from core.helpers import File
from application.repositories.clients.github.client import GitHubClient


class GitHubClientTestCase(unittest.TestCase):
    def setUp(self):
        self.client = GitHubClient()

    @httpretty.activate
    def test_ensure_that_prepare_items_of_search_repositories(self):
        response = File.read(os.path.dirname(__file__) + "/search_repositories.json")
        repositories_items = json.loads(response).get("items")
        httpretty.register_uri(httpretty.GET, "https://api.github.com/search/repositories", body=response, status=200)
        paginate, repositories = self.client.search_repositories("")
        repositories = {r.repository_id: r for r in repositories}
        for repository in repositories_items:
            repository_id = repository["id"]
            assert repositories[repository_id].name == repository["name"]
            assert repositories[repository_id].owner == repository["owner"]["login"]
            assert repositories[repository_id].description == repository["description"]
            assert repositories[repository_id].stars == repository["stargazers_count"]
            assert repositories[repository_id].language == repository["language"]
            assert repositories[repository_id].updated == repository["updated_at"]

    @httpretty.activate
    def test_ensure_that_search_repositories_does_not_crash_with_403(self):
        httpretty.register_uri(httpretty.GET, "https://api.github.com/search/repositories", body="", status=403)
        paginate, repositories = self.client.search_repositories("")
        assert repositories == []

    @httpretty.activate
    def test_ensure_that_search_repositories_does_not_crash_with_422(self):
        httpretty.register_uri(httpretty.GET, "https://api.github.com/search/repositories", body="", status=422)
        paginate, repositories = self.client.search_repositories("")
        assert repositories == []
