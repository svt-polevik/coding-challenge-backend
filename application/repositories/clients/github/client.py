from core.helpers import Pagination
from application.repositories.client import RepositoryClient
from application.repositories.domain import Repository, Repositories
from application.repositories.clients.github.api import GitHubAPI
from application.repositories.clients.github.exceptions import GitHubAPIRateLimitException, GitHubAPICountLimitException


class GitHubClient(RepositoryClient):
    def __init__(self, username=None, password=None, token=None):
        """
        :param username: login name
        :param password: password for the login
        :param token: OAuth token
        """
        self.api = GitHubAPI()
        self.api.authorize(username, password, token)

    @staticmethod
    def encode_repository(repository):
        """
        Serializer repository item from response to application domain entity

        :param repository: Item of search repositories response
        :param repository: dict[str, object]

        :rtype: application.repositories.domain.Repository
        """

        return Repository(
            repository_id=repository['id'], repository_type=Repositories.GITHUB, name=repository['name'],
            owner=repository['owner']['login'], description=repository['description'], language=repository['language'],
            updated=repository['updated_at'], stars=repository['stargazers_count']
        )

    def search_repositories(self, query, language=None, stars=None, sort=None, order=None, pagination=Pagination()):
        """
        :param query: The search keywords, can contain any qualifiers.
        :type query: str

        :param language: Searches repositories based on the language they're written in.
        :type language: str

        :param stars: Searches repositories based on the number of stars.
        :type stars: str

        :param sort: The sort field. One of stars, forks, or updated.
        :type sort: str

        :param order: The sort order if sort parameter is provided. One of asc or desc. Default: desc
        :type order: str

        :param pagination: The Pagination class to be used for paginating response
        :type pagination: core.helpers.Pagination

        :rtype: core.helpers.Pagination, list[application.repositories.domain.Repository]
        """
        repositories = []
        params = dict(language=language, stars=stars, sort=sort, order=order, per_page=pagination.per_page)
        try:
            for page in pagination:
                total, items = self.api.search_repositories(query, **{**dict(page=page), **params})
                repositories += items
                pagination.total = total
                pagination.count += len(items)
        except (GitHubAPIRateLimitException, GitHubAPICountLimitException):
            pass
        finally:
            repositories = [self.encode_repository(repository) for repository in repositories]
            return pagination, repositories
