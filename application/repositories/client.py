from core.helpers import Pagination


class RepositoryClient(object):

    def search_repositories(self, query, language=None, stars=None, sort=None, order=None, pagination=None):
        """
        :param query: The search keywords, can contain any qualifiers.
        :type query: str

        :param language: Searches repositories based on the language they're written in.
        :type language: str

        :param stars: Searches repositories based on the number of stars.
        :type stars: str

        :param sort: The sort field. One of stars, forks, or updated.
        :type sort: str

        :param order: The sort order if sort parameter is provided. One of asc or desc. Default: desc
        :type order: str

        :param pagination: The Pagination class to be used for paginating response
        :type pagination: core.helpers.Pagination

        :rtype: core.helpers.Pagination, list[application.repositories.domain.Repository]
        """
        raise NotImplementedError()


class RepositoriesClient(object):
    def __init__(self, clients):
        """
        :param clients: List with implements of repositories clients.
        :type clients: list[application.repositories.client.RepositoryClient]
        """
        self.clients = clients

    def search_repositories(self, query, language=None, stars=None, sort=None, order=None, pagination=None):
        """
        :param query: The search keywords, can contain any qualifiers.
        :type query: str

        :param language: Searches repositories based on the language they're written in.
        :type language: str

        :param stars: Searches repositories based on the number of stars.
        :type stars: str

        :param sort: The sort field. One of stars, forks, or updated.
        :type sort: str

        :param order: The sort order if sort parameter is provided. One of asc or desc. Default: desc
        :type order: str

        :param pagination: The Pagination class to be used for paginating response
        :type pagination: core.helpers.Pagination

        :rtype: list[application.repositories.domain.Repository]
        """
        repositories = []
        for repository in self.clients:
            try:
                params = dict(language=language, stars=stars, sort=sort, order=order, pagination=pagination)
                paginate, items = repository.search_repositories(query, **params)
                repositories += items
            except ValueError:
                raise
        return repositories
