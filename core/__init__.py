import os
import logging
from mongoengine import connect
from settings import CONFIG
connect(CONFIG.MONGO_DATABASE_NAME, host=CONFIG.MONGO_HOST, port=CONFIG.MONGO_PORT)

"""
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s')
handler = logging.Handler()
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)
"""
