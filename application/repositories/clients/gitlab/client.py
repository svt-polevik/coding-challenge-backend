from application.repositories.client import RepositoryClient


class GitLabClient(RepositoryClient):

    def search_repositories(self, query, sort=None, order=None, limit=100):
        """
        Find repositories via the Search API.

        The query can contain any combination of the following supported qualifers:

        :param query: a valid query as described above, e.g., ``tetris language:python``
        :type query: str

        :param sort: how the results should be sorted
        :type sort: str

        :param order: the direction of the sorted results, options: ``asc``, ``desc``; default: ``desc``
        :type order: str

        :rtype: list[services.gitlab.domain.GitLabRepository]
        """
        return []
