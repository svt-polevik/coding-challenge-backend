
import unittest

from core import CONFIG
from application.repositories.clients.github.api import GitHubAPI
from application.repositories.clients.github.exceptions import GitHubAPICountLimitException


class GitHubAPITestCase(unittest.TestCase):
    def setUp(self):
        self.api = GitHubAPI()
        self.api.authorize(token=CONFIG.GITHUB_ACCESS_TOKEN)

    def test_ensure_that_search_repositories_with_paginate(self):
        query = CONFIG.GITHUB_DEFAULT_SEARCH_QUERY
        for per_page in [50, 75, 100]:
            total_count, repositories = self.api.search_repositories(query=query, page=1, per_page=per_page)
            assert len(repositories) == per_page
            assert total_count > per_page

    def test_ensure_that_search_repositories_raise_count_exception(self):
        with self.assertRaises(GitHubAPICountLimitException):
            self.api.search_repositories(query="", page=1000)
