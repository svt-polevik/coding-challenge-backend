from settings.environment import EnvironmentConfiguration


class StagingConfiguration(EnvironmentConfiguration):
    DEBUG = True
    HOST = '0.0.0.0'
    PORT = 9090
    MONGO_HOST = '127.0.0.1'
    MONGO_PORT = 27017
    MONGO_DATABASE_NAME = 'repositories_development'
    GITHUB_DEFAULT_SEARCH_QUERY = 'language:python stars:>500'
    GITHUB_ACCESS_TOKEN = 'e05ec373844e452d614fd4c808ee675cd358fd5c'
