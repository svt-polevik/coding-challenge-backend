from core.helpers import Pagination


class RepositoriesModel(object):
    def __init__(self, repositories_dao):
        """
        :param repositories_dao: Instance of DAO for work with repositories collection
        :type repositories_dao: application.repositories.dao.RepositoriesDAO
        """
        self.repositories_dao = repositories_dao

    def get_recent(self, sort=None, order=None, pagination=Pagination()):
        """
        :param pagination: param for controling paginate of results
        :type pagination: core.helpers.Pagination

        :return: list of repository items with matches by keyword
        :rtype: list[application.repositories.domain.Repository]
        """
        total, recents = self.repositories_dao.get_recent(skip=pagination.skip, limit=pagination.limit)
        return pagination(total=total, count=len(recents)), recents

    def get_by_id(self, repository_id):
        """
        :param repository_id: unique identifier of remote repository
        :type repository_id: str

        :rtype: list[application.partials.repositories.domain.Repository]
        """
        return self.repositories_dao.get_by_id(repository_id)

    def get_by_name(self, keyword, pagination=None):
        """
        :param keyword: query word string or regular expression for matching the search.
        :type keyword: string|regexp

        :param pagination: param for controling paginate of results
        :type pagination: helpers.Pagination

        :return: list of repository items with matches by keyword
        :rtype: list[application.partials.repositories.domain.Repository]
        """
        return self.repositories_dao.get_by_name(keyword, pagination=pagination)

    def search(self, keyword,  skip=None, limit=None):
        """
        Method for full-text searches. Dependent by implementation of DAO and database engine.

        :param keyword: keyword for text search queries on string content
        :type keyword: string

        :param pagination: param for controling paginate of results
        :type pagination: helpers.Pagination

        :return: list[application.repositories.domain.Repository]
        """
        return self.repositories_dao.search(keyword, skip=skip, limit=limit)

    def upsert(self, repositories):
        """
        Method for multiple insertion repositories records. Update if record exists and adds if no records were found.

        :param repositories: list of repositories to be added \ updated
        :type repositories: list[application.repositories.domain.Repository]

        :rtype: list[application.repositories.domain.Repository]
        """
        return [self.repositories_dao.upsert_one(repository) for repository in repositories]

    def upsert_one(self, repository):
        """
        Method for single insertion repositories records. Update if record exists and adds if no records were found.

        :param repository: Item of repositories to be added \ updated
        :type repository: application.repositories.domain.Repository

        :rtype: application.repositories.domain.Repository
        """
        return self.repositories_dao.upsert_one(repository)

