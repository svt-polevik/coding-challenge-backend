from application.repositories.routes import WebRequestRepositoriesRules
from flask import Flask, request, Response, has_request_context
from core import CONFIG

app = Flask(CONFIG.APP_NAME)


def _responder(status=200, mimetype=None, body=None):
    return Response(response=body, status=status, mimetype=mimetype)


def _requester():
    return {k: request.args.get(k, type=None, default=None) for k in request.args}


for rule, handler in WebRequestRepositoriesRules:
    app.add_url_rule(rule, rule, handler.handler(_requester, _responder))
