import requests

from application.repositories.clients.github.exceptions import GitHubAPIRateLimitException, GitHubAPICountLimitException


class GitHubAPI(object):
    def __init__(self):
        self.endpoint = "https://api.github.com"
        self.session = requests.Session()

    def authorize(self, username=None, password=None, token=None):
        """
        Username and password combination or a token, none of the parameters are required.
        If you provide none of them, you will receive ``None``.

        :param username: login name
        :param password: password for the login
        :param token: OAuth token
        """
        if username and password:
            self.session.auth = (username, password)
        elif token:
            self.session.headers.update(dict(Authorization="Token %s" % token))

    @staticmethod
    def _build_qualifiers_string(language=None, stars=None):
        """
        :param language: Searches repositories based on the language they're written in.
        :type language: str|None

        :param stars: Searches repositories based on the number of stars.
        :type stars: str|None

        :return: Query parameter with qualifiers modifiers.
        :rtype: str
        """
        qualifiers = {"language": language, "stars": stars}
        return " ".join("{!s}:{!s}".format(k, qualifiers[k]) for k in qualifiers if qualifiers[k] is not None)

    def search_repositories(self, query, language=None, stars=None, sort=None, order=None, page=1, per_page=100):
        """
        :param query: The search keywords, can contain any qualifiers.
        :type query: str|None

        :param language: Searches repositories based on the language they're written in.
        :type language: str|None

        :param stars: Searches repositories based on the number of stars.
        :type stars: str|None

        :param sort: The sort field. One of stars, forks, or updated.
        :type sort: str|None

        :param order: The sort order if sort parameter is provided. One of asc or desc. Default: desc
        :type order: str|None

        :param page: Number of current page of search response.
        :type page: int

        :param per_page:  count of items by search response. Default 30 items by default. Maximum: 100
        :type per_page: int

        :rtype: core.helpers.Pagination, list[application.repositories.domain.Repository]
        """
        qualifiers = self._build_qualifiers_string(language=language, stars=stars)
        params = {"q": "%s %s" % (query, qualifiers), "sort": sort, "page": page, "per_page": per_page}
        response = self.session.get(self.endpoint + "/search/repositories", params=params)

        if response.status_code == 200:
            result = response.json()
            return (result[_] for _ in ["total_count", "items"] if _ in result)

        if response.status_code == 422:
            raise GitHubAPICountLimitException()

        if response.status_code == 403:
            raise GitHubAPIRateLimitException()

        response.raise_for_status()
