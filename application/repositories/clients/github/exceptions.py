from core.exceptions import APIRateLimitException, APICountLimitException


class GitHubAPIRateLimitException(APIRateLimitException):
    pass


class GitHubAPICountLimitException(APICountLimitException):
    pass
