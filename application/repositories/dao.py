from mongoengine import *

from application.repositories.domain import Repository, Repositories


class RepositoriesDB(Document):
    """
    Specification of store repositories records.
    """
    repository_id = StringField(required=True, unique=True)
    repository_type = IntField(required=True)
    owner = StringField(required=True, max_length=100)
    name = StringField(required=True, max_length=200)
    description = StringField()
    stars = IntField(required=True)
    language = StringField(null=True)
    updated = StringField(null=False)
    meta = {
        'collection': 'repositories',
        'indexes': [{
            'fields': ['$name', '$owner', '$description'],
            'default_language': 'english',
            'language_override': 'DECLINE',
            'weights': {'name': 10, 'description': 2}
        }]
    }


class RepositoriesDAO(object):
    def __init__(self): pass

    @staticmethod
    def decode(repository):
        """
        :param repository: Notation of MongoDB repository document
        :type repository: application.partials.repositories.dao.RepositoriesDB

        :rtype: application.repositories.domain.Repository
        """
        return Repository(repository_id=repository.repository_id, name=repository.name, owner=repository.owner,
                          repository_type=Repositories(repository.repository_type), description=repository.description,
                          stars=repository.stars, language=repository.language, updated=repository.updated)

    def get_by_id(self, repository_id):
        """
        :param repository_id: identifier of local mongodb repository record
        :type repository_id: bson.ObjectId|string

        :rtype: application.repositories.domain.Repository
        """
        return self.decode(RepositoriesDB.objects(id=repository_id).first())

    def get_recent(self, skip=None, limit=None):
        """
        Method for get all recent repositories without clause but with sort and order params.

        :param skip: the number of results to skip
        :type skip: int|None

        :param limit: the number of results to return
        :param limit: int|None

        :rtype: list[application.repositories.domain.Repository]
        """
        return RepositoriesDB.objects.count(), \
               [self.decode(repo) for repo in RepositoriesDB.objects.skip(skip).limit(limit)]

    def search(self, keyword, skip=None, limit=None):
        """
        Method for full-text searches.

        :param keyword: query word string or regular expression for matching the search.
        :type keyword: string|regexp

        :param skip: the number of results to skip
        :type skip: int|None

        :param limit: the number of results to return
        :param limit: int|None

        :rtype: list[application.repositories.domain.Repository]
        """
        return [self.decode(repo) for repo in RepositoriesDB.objects.search_text(keyword).skip(skip).limit(limit)]

    def upsert_one(self, repository):
        """
        :param repository: RepositorySpec object for updating or adds in store
        :type repository: application.repositories.domain.Repository

        :rtype: application.repositories.domain.Repository
        """
        upserted = RepositoriesDB.objects(repository_id=repository.repository_id).upsert_one(**repository.json())
        return upserted and self.decode(upserted)
