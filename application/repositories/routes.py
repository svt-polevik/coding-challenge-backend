from application.repositories.handlers import OnGetRecent, OnGetSearch, OnGetSearchRemote
from core.tools import WebHandler


WebRequestRepositoriesRules = [
    ("/recent", WebHandler(onget=OnGetRecent)),
    ("/search", WebHandler(onget=OnGetSearch)),
    ("/search/remote", WebHandler(onget=OnGetSearchRemote))
]

CommandLineRepositoriesRules = {
    "recent": lambda args: OnGetRecent.dispatch(args),
    "search": lambda args: OnGetSearch.dispatch(args),
    "search_remote": lambda args: OnGetSearchRemote.dispatch(args)
}
