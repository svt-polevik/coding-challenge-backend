import datetime
from factory import Factory
from factory.fuzzy import FuzzyInteger, FuzzyText, FuzzyChoice, FuzzyDate

from application.repositories.domain import Repository


class RepositoryFactory(Factory):
    class Meta:
        model = Repository

    repository_id = FuzzyInteger(100)
    repository_type = FuzzyChoice([1, 2])
    name = FuzzyText(prefix="name_")
    owner = FuzzyText(prefix="owner_")
    description = FuzzyText(length=100)
    stars = FuzzyInteger(500)
    language = FuzzyChoice(["Python", "Java"])
    updated = FuzzyDate(datetime.date(2018, 1, 1))
