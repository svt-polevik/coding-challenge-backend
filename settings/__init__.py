import os
from settings.development import DevelopmentConfiguration as CONFIG
if os.environ.get("env") == "development":
    from settings.development import DevelopmentConfiguration as CONFIG
elif os.environ.get("env") == "staging":
    from settings.staging import StagingConfiguration as CONFIG
