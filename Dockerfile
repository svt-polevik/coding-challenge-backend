FROM python:3
ADD . /mnt/application
WORKDIR /mnt/application
RUN easy_install virtualenv pip
RUN virtualenv env
RUN /bin/bash -c "source env/bin/activate"
RUN pip install -r requirements.txt
RUN python -m pytest