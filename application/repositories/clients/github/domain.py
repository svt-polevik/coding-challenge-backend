class GitHubRepository(object):
    def __init__(self, repository_id, owner, name, description, stars, language, updated_at):
        """
        :param repository_id: identifier of remote github repository.
        :type repository_id: int

        :param owner: owner of the repository.
        :type owner: str

        :param name: name of the repository. ex: owner/`repository`.
        :type name: str

        :param description: short specification information about repository.
        :type description: str

        :param stars: number of stars for repository.
        :type stars: int

        :param language: repository language code. ex `Python` or `Java`
        :param language: str

        :param updated_at: date of last update repository.
        :param updated_at: datetime
        """
        self.repository_id = repository_id
        self.owner = owner
        self.name = name
        self.description = description
        self.stars = stars
        self.language = language
        self.updated_at = updated_at
