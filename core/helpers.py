from collections import Iterable
from core.validators import PaginationValidator, OrderSortGroupValidator


class File(object):
    @staticmethod
    def is_exist(path):
        pass

    @classmethod
    def read(cls, path):
        with open(path, 'r') as filehandle:
            content = filehandle.read().splitlines()
            filehandle.close()
            return "".join(content)


class OrderSortGroup(object):
    """
    A Pagination object to be used for querying and displaying pagination links on frontend
    """
    def __init__(self, order="ASC", sort=[], group=[]):
        self.order = order
        self.sort = sort
        self.group = group

    @staticmethod
    def validate(params, fields=[]):
        validator = OrderSortGroupValidator.validate(params, fields=fields)
        return validator.


class Pagination(Iterable):
    """
    A Pagination class to be used for paginate objects records
    """

    def __init__(self, page=1, per_page=None, skip=0, limit=100, total=0, count=0):
        self.page = page
        self.per_page = per_page
        self.skip = skip
        self.limit = limit
        self.total = total
        self.count = count

    def __call__(self, total=None, count=None):
        if total:
            self.total = total
        if count:
            self.count = count
        self._calculate()
        return self

    def __iter__(self):
        self.page -= 1
        return self

    def __next__(self):
        self.page += 1
        if self.count and self.limit and self.count >= self.limit:
            raise StopIteration()
        elif self.total > 0 and self.per_page and self.per_page > self.count:
            raise StopIteration()
        else:
            return self.page

    def __repr__(self):
        return "<Pagination(total=%r, count=%r, limit=%r, pages=%r>" % (self.total, self.count, self.limit, self.pages)

    def _calculate(self):
        if self.per_page and self.limit < self.per_page:
            self.per_page = self.limit

        if self.per_page and self.count < self.per_page:
            self.per_page = self.count

    @property
    def pages(self):
        return self.count // self.per_page

    @staticmethod
    def validate(params):
        return Pagination(**PaginationValidator.validate(params))

    def json(self):
        return vars(self)
