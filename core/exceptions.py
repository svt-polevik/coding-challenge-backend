class ValidationError(Exception):
    def __init__(self, parameter=None, error=None):
        self.parameter = parameter
        self.error = error

    @property
    def message(self):
        if self.parameter and self.error:
            return "parameter: %s missing. %s" % (str(self.parameter), str(self.error))


class APIRateLimitException(Exception):
    pass


class APICountLimitException(Exception):
    pass
