import json
from core.exceptions import ValidationError


class Handler(object):
    @staticmethod
    def decode(params):
        raise NotImplementedError()

    @staticmethod
    def handle(params):
        raise NotImplementedError()

    @staticmethod
    def encode(result):
        raise NotImplementedError()

    @classmethod
    def dispatch(cls, params):
        try:
            decoded = cls.decode(params)
            result = cls.handle(decoded)
            encoded = cls.encode(result)
            return encoded
        except ValidationError:
            raise


class WebHandler(object):
    def __init__(self, onget=None, onpost=None, onput=None, ondelete=None):
        self.onget = onget
        self.onpost = onpost
        self.onput = onput
        self.ondelete = ondelete

    def handler(self, requester, responder):
        def dispatch():
            try:
                body = self.onget.dispatch(requester())
            except ValidationError as exception:
                body = json.dumps({"error": exception.message}, indent=4)
                return responder(status=400, mimetype='application/json', body=body)
            else:
                if type(body) is str:
                    return responder(status=200, mimetype='text/html', body=body)
                elif type(body) is dict:
                    return responder(status=200, mimetype='application/json', body=json.dumps(body, indent=4))
        return dispatch
